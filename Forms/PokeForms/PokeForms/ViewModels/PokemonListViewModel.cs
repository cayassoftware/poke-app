﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using PokeApi;
using Xamarin.Forms;

namespace PokeForms.ViewModels
{
    public class PokemonListViewModel : BaseViewModel
    {
        private PokeApiClient _pokeApiClient = new PokeApiClient();

        public ObservableCollection<Pokemon> Pokemons { get; set; }
        public Command LoadItemsCommand { get; set; }

        public PokemonListViewModel()
        {
            Title = "Pokemons";
            Pokemons = new ObservableCollection<Pokemon>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadPokemonCommand());
        }

        async Task ExecuteLoadPokemonCommand()
        {
            IsBusy = true;

            try
            {
                Pokemons.Clear();

                var pokemons = await _pokeApiClient.GetPokemons(151);

                foreach (var pokemon in pokemons)
                {
                    Pokemons.Add(pokemon);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}