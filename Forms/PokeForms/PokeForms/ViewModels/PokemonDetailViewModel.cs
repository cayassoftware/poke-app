﻿using System;
using System.Linq;
using PokeApi;

namespace PokeForms.ViewModels
{
    public class PokemonDetailViewModel : BaseViewModel
    {
        public Pokemon Pokemon { get; set; }

        public string Name { get; set; }

        public string ImageName { get; set; }

        public string Types { get; set; }

        public PokemonDetailViewModel(Pokemon pokemon)
        {
            Title = pokemon.Name;
            Pokemon = pokemon;

            Name = pokemon.Name;
            ImageName = pokemon.Sprites.FrontDefault;
            Types = string.Join(", ", pokemon.Types.Select(t => t.Details.Name));
        }
    }
}
