﻿using System;
using Xamarin.Forms;
using PokeForms.ViewModels;

namespace PokeForms.Views
{
    public partial class PokemonDetailPage : ContentPage
    {
        PokemonDetailViewModel viewModel;

        public PokemonDetailPage(PokemonDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }
    }
}