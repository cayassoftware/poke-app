﻿using System;
using PokeApi;
using PokeForms.ViewModels;
using Xamarin.Forms;

namespace PokeForms.Views
{
    public partial class PokemonListPage : ContentPage
    {
        PokemonListViewModel viewModel;

        public PokemonListPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new PokemonListViewModel();
        }

        async void OnItemSelected(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var pokemon = (Pokemon)layout.BindingContext;

            await Navigation.PushAsync(new PokemonDetailPage(new PokemonDetailViewModel(pokemon)));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Pokemons.Count == 0)
                viewModel.IsBusy = true;
        }
    }
}