﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace PokeApi
{
    public class PokeApiClient
    {
        private readonly HttpClient _client;

        public PokeApiClient()
        {
            _client = new HttpClient { BaseAddress = new Uri("https://pokeapi.co/api/v2/") };
        }

        public async Task<List<Pokemon>> GetPokemons(int limit)
        {
            var response = await _client.GetAsync($"pokemon?limit={limit}").ConfigureAwait(false);

            response.EnsureSuccessStatusCode();

            var overview = JsonConvert.DeserializeObject<PokemonOverview>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));

            var tasks = (overview.Results.Select(overviewResult => GetPokemon(overviewResult.Name))).ToList();

            await Task.WhenAll(tasks);

            var pokemons = new List<Pokemon>();

            foreach (var item in tasks)
            {
                if (item.Status != TaskStatus.RanToCompletion)
                    continue;

                pokemons.Add(item.Result);
            }

            return pokemons;
        }

        public async Task<Pokemon> GetPokemon(int id)
        {
            var response = await _client.GetAsync($"pokemon/{id}").ConfigureAwait(false);

            response.EnsureSuccessStatusCode();


            return JsonConvert.DeserializeObject<Pokemon>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
        }

        public async Task<Pokemon> GetPokemon(string name)
        {
            var response = await _client.GetAsync($"pokemon/{name}");

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Pokemon>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
        }

        public Task<System.IO.Stream> GetPokemonImage(string url)
        {
            return _client.GetStreamAsync(url);
        }
    }
}

