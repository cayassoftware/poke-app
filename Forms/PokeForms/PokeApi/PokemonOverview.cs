﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PokeApi
{
    public class Result
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class PokemonOverview
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("results")]
        public List<Result> Results { get; set; }
    }
}
