﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using PokeApi;

namespace PokeDroid.Adapter
{
    public class PokemonAdapter : RecyclerView.Adapter
    {
        private List<Pokemon> _pokemons;
        private PokeApiClient _client;

        //Step 2
        public event EventHandler<int> ItemClick;


        public PokemonAdapter(List<Pokemon> pokemons)
        {
            _pokemons = pokemons;
            _client = new PokeApiClient();
        }

        public override int ItemCount => _pokemons.Count;

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var listItemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.PokemonListItem, parent, false);

            //Step 1
            //return new PokemonListViewHolder(listItemView);

            //Step 2
            return new PokemonListViewHolder(listItemView, OnClick);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var pokeViewHolder = holder as PokemonListViewHolder;
            pokeViewHolder.Name.Text = _pokemons[position].Name;
            pokeViewHolder.Types.Text = string.Join(", ", _pokemons[position].Types.Select(t => t.Details.Name));

            Task.Run(async () =>
            {
                using (var imageStream = await _client.GetPokemonImage(_pokemons[position].Sprites.FrontDefault))
                using (var bitmap = await BitmapFactory.DecodeStreamAsync(imageStream))
                {
                    pokeViewHolder.Image.SetImageBitmap(bitmap);
                }
            });
        }

        public void Update(List<Pokemon> pokemons)
        {
            _pokemons = pokemons;
            NotifyDataSetChanged();
        }

        //Step 2
        private void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }

        public class PokemonListViewHolder : RecyclerView.ViewHolder
        {
            public TextView Name { get; set; }
            public TextView Types { get; set; }
            public ImageView Image { get; set; }

            //Step 1
            public PokemonListViewHolder(View itemView) : base(itemView)
            {
                Name = itemView.FindViewById<TextView>(Resource.Id.PokeList_Item_Name);
                Types = itemView.FindViewById<TextView>(Resource.Id.PokeList_Item_Types);
                Image = itemView.FindViewById<ImageView>(Resource.Id.PokeList_Item_Image);
            }

            //Step 2
            public PokemonListViewHolder(View itemView, Action<int> action) : base(itemView)
            {
                Name = itemView.FindViewById<TextView>(Resource.Id.PokeList_Item_Name);
                Types = itemView.FindViewById<TextView>(Resource.Id.PokeList_Item_Types);
                Image = itemView.FindViewById<ImageView>(Resource.Id.PokeList_Item_Image);

                itemView.Click += (sender, e) => action(LayoutPosition);
            }
        }
    }
}