﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using PokeApi;
using PokeDroid.Adapter;

namespace PokeDroid
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private PokeApiClient _pokeApiClient;
        private List<Pokemon> _pokemons;
        private PokemonAdapter _adapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_main);

            _pokeApiClient = new PokeApiClient();
            _adapter = new PokemonAdapter(new List<Pokemon>());

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            var recyclerView = FindViewById<RecyclerView>(Resource.Id.PokeList_RecyclerView);
            recyclerView.SetAdapter(_adapter);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this));

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;

            FabOnClick(this, EventArgs.Empty);
        }

        //Step 2
        protected override void OnResume()
        {
            base.OnResume();

            _adapter.ItemClick += PokemonAdapter_ItemClick;
        }

        //Step 2
        protected override void OnPause()
        {
            _adapter.ItemClick -= PokemonAdapter_ItemClick;

            base.OnPause();
        }

        //Step 2
        private void PokemonAdapter_ItemClick(object sender, int e)
        {
            var intent = new Intent(this, typeof(DetailActivity));
            intent.PutExtra("Pokemon", Newtonsoft.Json.JsonConvert.SerializeObject(_pokemons[e]));

            StartActivity(intent);
        }

        private async void FabOnClick(object sender, EventArgs eventArgs)
        {
            _pokemons = await _pokeApiClient.GetPokemons(15);
            _adapter.Update(_pokemons);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
