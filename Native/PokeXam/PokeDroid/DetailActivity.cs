﻿using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using PokeApi;

namespace PokeDroid
{
    [Activity(Theme = "@style/AppTheme.NoActionBar")]
    public class DetailActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.PokemonDetails);

            if (string.IsNullOrWhiteSpace(Intent.GetStringExtra("Pokemon")))
            {
                Toast.MakeText(this, "Pokemon gewählt", ToastLength.Short).Show();
                Finish();
            }

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);

            var pokemon = JsonConvert.DeserializeObject<Pokemon>(Intent.GetStringExtra("Pokemon"));

            Title = pokemon.Name;

            var nameTextView = FindViewById<TextView>(Resource.Id.PokeDetails_Name);
            nameTextView.Text = pokemon.Name;

            var typesTextView = FindViewById<TextView>(Resource.Id.PokeDetails_Types);
            typesTextView.Text = string.Join(", ", pokemon.Types.Select(t => t.Details.Name));

            LoadImage(pokemon.Sprites.FrontDefault);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
                Finish();

            return base.OnOptionsItemSelected(item);
        }

        private async Task LoadImage(string url)
        {
            var client = new PokeApiClient();

            using (var imageStream = await client.GetPokemonImage(url))
            using (var bitmap = await BitmapFactory.DecodeStreamAsync(imageStream))
            {
                var imageView = FindViewById<ImageView>(Resource.Id.PokeDetails_ImageView);
                imageView.SetImageBitmap(bitmap);
            }
        }
    }
}
