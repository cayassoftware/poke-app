﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using PokeApi;
using System.Threading.Tasks;

namespace PokeApple
{
    public partial class MasterViewController : UITableViewController
    {
        private DataSource _dataSource;

        protected MasterViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = NSBundle.MainBundle.GetLocalizedString("Master", "Master");
            SplitViewController.PreferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible;

            TableView.Source = _dataSource = new DataSource(new List<Pokemon>());

            var pokeApiClient = new PokeApiClient();
            pokeApiClient.GetPokemons(13).ContinueWith(t =>
            {
                _dataSource.UpdatePokemons(t.Result);
                TableView.ReloadData();

            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            if (segue.Identifier == "showDetail")
            {
                var controller = (DetailViewController)((UINavigationController)segue.DestinationViewController).TopViewController;
                controller.SetDetailItem(_dataSource.Pokemons[TableView.IndexPathForSelectedRow.Row]);
                controller.NavigationItem.LeftBarButtonItem = SplitViewController.DisplayModeButtonItem;
                controller.NavigationItem.LeftItemsSupplementBackButton = true;
            }
        }


        class DataSource : UITableViewSource
        {
            static readonly NSString CellIdentifier = new NSString("Cell");

            public IList<Pokemon> Pokemons { get; private set; }

            public DataSource(List<Pokemon> pokemons)
            {
                Pokemons = pokemons;
            }

            // Customize the number of sections in the table view.
            public override nint NumberOfSections(UITableView tableView)
            {
                return 1;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return Pokemons.Count;
            }

            // Customize the appearance of table view cells.
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var cell = tableView.DequeueReusableCell(CellIdentifier, indexPath);
                cell.TextLabel.Text = Pokemons[indexPath.Row].Name;

                //var cell = tableView.DequeueReusableCell(PokeCell.Key) as PokeCell ?? new PokeCell();
                //cell.Name.Text = Pokemons[indexPath.Row].Name;

                //Task.Run(async () =>
                //{
                //    var client = new PokeApiClient();

                //    using (var imageStream = await client.GetPokemonImage(Pokemons[indexPath.Row].Sprites.FrontDefault))
                //    using (var imageData = NSData.FromStream(imageStream))
                //    {
                //        cell.Image.Image = UIImage.LoadFromData(imageData);
                //        tableView.ReloadRows(new[] { indexPath }, UITableViewRowAnimation.None);
                //    }
                //});

                return cell;
            }

            public void UpdatePokemons(List<Pokemon> pokemons)
            {
                Pokemons = pokemons;
            }
        }
    }
}
