﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Foundation;
using PokeApi;
using UIKit;

namespace PokeApple
{
    public partial class DetailViewController : UIViewController
    {
        public Pokemon DetailItem { get; set; }

        public DetailViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            ConfigureView();
        }

        public void SetDetailItem(Pokemon pokemon)
        {
            if (DetailItem != pokemon)
            {
                DetailItem = pokemon;

                // Update the view
                ConfigureView();
            }
        }

        void ConfigureView()
        {
            // Update the user interface for the detail item
            if (IsViewLoaded && DetailItem != null)
            {
                detailDescriptionLabel.Text = DetailItem.Name;
                typesLabel.Text = string.Join(", ", DetailItem.Types.Select(t => t.Details.Name));

                LoadImage(DetailItem.Sprites.FrontDefault).ContinueWith(t =>
                {
                    pokeImageView.Image = t.Result;

                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        private async Task<UIImage> LoadImage(string url)
        {
            var client = new PokeApiClient();

            using (var imageStream = await client.GetPokemonImage(url))
            using (var imageData = NSData.FromStream(imageStream))
            {
                return UIImage.LoadFromData(imageData);
            }
        }
    }
}