﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace PokeApple
{
    public class PokeCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("PokeTableViewCell");

        public static int CELLHEIGHT = 53;

        public UILabel Name { get; set; }

        public UIImageView Image { get; set; }


        public PokeCell() : base(UITableViewCellStyle.Default, Key)
        {
            SelectionStyle = UITableViewCellSelectionStyle.Gray;

            CGRect frame = Frame;
            frame.Height = CELLHEIGHT;
            Frame = frame;

            ContentView.BackgroundColor = UIColor.FromRGB(255, 255, 255);

            Image = new UIImageView();

            ContentView.AddSubview(Image);

            Name = new UILabel();
            Name.Font = UIFont.BoldSystemFontOfSize(19f);

            ContentView.AddSubview(Name);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            Image.Frame = new CGRect(50, CELLHEIGHT / 2 - 17 / 2, 31, 17);
            Name.Frame = new CGRect(89, CELLHEIGHT / 2 - 25 / 2, ContentView.Frame.Width - 89 - 40, 25);
        }
    }
}
